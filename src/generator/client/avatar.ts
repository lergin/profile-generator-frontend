import { EXIFOrientation } from "../../util";
import { CanvasLayer } from "./Layers";
import { TemplateRenderFunction } from "./clientGenerator";

export type AvatarOptions = {
  width: number;
  height: number;
  template: string;
  src: string;
  srcRotation: EXIFOrientation;
  srcOffsetX: number;
  srcOffsetY: number;
  zoomFactor: number;
};

export const squareImageAvatar: TemplateRenderFunction = function({
  template,
  width,
  height,
  src,
  srcRotation
}: AvatarOptions): CanvasLayer[] {
  const BASE_SIZE = 1200;
  const BORDER_WIDTH = 176;

  const size = Math.max(width, height);

  return [
    {
      type: "IMAGE_SQUARE",
      offset: BORDER_WIDTH * (size / BASE_SIZE),
      size: size - BORDER_WIDTH * 2 * (size / BASE_SIZE),
      src,
      rotation: srcRotation
    },
    {
      type: "IMAGE_SQUARE",
      offset: 0,
      size: size,
      src: template,
      rotation: 1
    }
  ];
};
