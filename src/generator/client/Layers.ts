import { ImageLayer, drawImage } from "./layer/ImageLayer";
import { SquareImageLayer, drawSquareImage } from "./layer/SquareImageLayer";
import { RectangleLayer, drawRectangle } from "./layer/RectangleLayer";
import { CircleLayer, drawCircle } from "./layer/CircleLayer";
import { TextCircleLayer, drawTextCircle } from "./layer/TextCircleLayer";
import { Ctx } from "./clientGenerator";
import { LayerDrawer } from "./layer/Layer";

export type CanvasLayer =
  | RectangleLayer
  | CircleLayer
  | ImageLayer
  | SquareImageLayer
  | TextCircleLayer;

export const drawLayer: LayerDrawer<CanvasLayer> = (
  ctx: Ctx,
  layer: CanvasLayer,
  width: number,
  height: number
) => {
  switch (layer.type) {
    case "RECTANGLE":
      return drawRectangle(ctx, layer, width, height);
    case "CIRCLE":
      return drawCircle(ctx, layer, width, height);
    case "IMAGE_SQUARE":
      return drawSquareImage(ctx, layer, width, height);
    case "IMAGE":
      return drawImage(ctx, layer, width, height);
    case "TEXT_CIRCLE":
      return drawTextCircle(ctx, layer, width, height);
  }
};
