import { ColorLayer } from "./ColorLayer";
import { LayerDrawer } from "./Layer";

export type CircleLayer = {
  type: "CIRCLE";
  center: number;
  width: number;
  radius: number;
} & ColorLayer;

export const drawCircle: LayerDrawer<CircleLayer> = (
  ctx,
  { center, radius, width, color }
) => {
  ctx.save();
  ctx.strokeStyle = color;
  ctx.lineWidth = width;
  ctx.beginPath();
  ctx.arc(center, center, radius, 0, Math.PI * 2, true);
  ctx.stroke();
  ctx.restore();
};
